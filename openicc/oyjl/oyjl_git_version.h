/** @file  oyjl_git_version.h
 *  @brief automatic generated variables
 */
#ifndef OYJL_GIT_VERSION_H
#define OYJL_GIT_VERSION_H

#define OYJL_GIT_VERSION               "4299-g2916472d6-2024-11-19"  /**< git describe --always HEAD  + date */ 

#endif /*OYJL_GIT_VERSION_H*/

