<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/About.qml" line="65"/>
        <source>Oyjl Options Renderer</source>
        <translation>Oyjl Optionen Betrachter</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="67"/>
        <source>All Rights reserved.</source>
        <translation>Alle Rechte reserviert</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="71"/>
        <source>Platform</source>
        <translation>Plattform</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="73"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="75"/>
        <source>Qt Version</source>
        <translation>Qt Version</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="88"/>
        <source>Launched app for </source>
        <translation>Starte Programm für </translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="99"/>
        <source>OK</source>
        <translation>Ja</translation>
    </message>
</context>
<context>
    <name>AppData</name>
    <message>
        <location filename="../src/app_data.cpp" line="84"/>
        <source>Json is invalid</source>
        <translation>Json is ungültig</translation>
    </message>
    <message>
        <location filename="../src/app_data.cpp" line="144"/>
        <source>finished loading</source>
        <translation>Laden beendet</translation>
    </message>
    <message>
        <location filename="../src/app_data.cpp" line="144"/>
        <source>size</source>
        <translation>Größe</translation>
    </message>
    <message>
        <location filename="../src/app_data.cpp" line="297"/>
        <source>%1 Version</source>
        <translation>%1 Version</translation>
    </message>
    <message>
        <source>NONE</source>
        <translation type="vanished">NICHTS</translation>
    </message>
    <message>
        <location filename="../src/app_data.cpp" line="675"/>
        <source>Permission not granted: </source>
        <translation>Berechtigung nicht gewährt: </translation>
    </message>
</context>
<context>
    <name>AppWindow</name>
    <message>
        <location filename="../qml/AppWindow.qml" line="126"/>
        <location filename="../qml/AppWindow.qml" line="166"/>
        <source>discharging</source>
        <translation>entladend</translation>
    </message>
    <message>
        <location filename="../qml/AppWindow.qml" line="126"/>
        <source>charging</source>
        <translation>aufladend</translation>
    </message>
    <message>
        <location filename="../qml/AppWindow.qml" line="126"/>
        <source>full</source>
        <translation>voll</translation>
    </message>
    <message>
        <location filename="../qml/AppWindow.qml" line="127"/>
        <location filename="../qml/AppWindow.qml" line="166"/>
        <source>Battery</source>
        <translation>Batterie</translation>
    </message>
    <message>
        <location filename="../qml/AppWindow.qml" line="146"/>
        <source>Night</source>
        <translation>Nacht</translation>
    </message>
    <message>
        <location filename="../qml/AppWindow.qml" line="148"/>
        <source>Day</source>
        <translation>Tag</translation>
    </message>
    <message>
        <location filename="../qml/AppWindow.qml" line="166"/>
        <source>not used</source>
        <translation>nicht benutzt</translation>
    </message>
    <message>
        <location filename="../qml/AppWindow.qml" line="186"/>
        <source>Busy</source>
        <translation>Beschäftigt</translation>
    </message>
    <message>
        <location filename="../qml/AppWindow.qml" line="456"/>
        <source>Ok</source>
        <translation>Ja</translation>
    </message>
</context>
<context>
    <name>LSwitch</name>
    <message>
        <location filename="../qml/LSwitch.qml" line="85"/>
        <source>Ok</source>
        <translation>Ja</translation>
    </message>
</context>
<context>
    <name>OptionsList</name>
    <message>
        <location filename="../qml/OptionsList.qml" line="140"/>
        <location filename="../qml/OptionsList.qml" line="193"/>
        <location filename="../qml/OptionsList.qml" line="237"/>
        <location filename="../qml/OptionsList.qml" line="259"/>
        <source>selected</source>
        <translation>ausgewählt</translation>
    </message>
    <message>
        <location filename="../qml/OptionsList.qml" line="140"/>
        <location filename="../qml/OptionsList.qml" line="193"/>
        <location filename="../qml/OptionsList.qml" line="237"/>
        <source>new/old</source>
        <translation>neu/alt</translation>
    </message>
    <message>
        <location filename="../qml/OptionsList.qml" line="422"/>
        <source>Multiple Options Hint</source>
        <translation>Hinweis für Mehrfachoptionen</translation>
    </message>
    <message>
        <location filename="../qml/OptionsList.qml" line="424"/>
        <source>Enter first value, append &apos;;&apos; and confirm. The final &apos;;&apos; is not executed. Then select next value. Repeat as needed. A final confirm without ending &apos;;&apos; can execute.</source>
        <translation>Zunächst einen Wert eingeben, dann ein Semikolon &apos;;&apos; anhängen und bestätigen. Anschließend den nächsten Wert wählen. Das Auswählen bei Bedarf wiederholen. Ein Bestätigen ohne Semikolon  &apos;;&apos; am Ende kann den Befehl ausführen.</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="41"/>
        <source>Loaded</source>
        <translation>Geladen</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="100"/>
        <source>commands enabled</source>
        <translation>Kommandos an</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="347"/>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="788"/>
        <location filename="../qml/main.qml" line="839"/>
        <location filename="../qml/main.qml" line="872"/>
        <source>Launched app for </source>
        <translation>Starte Programm für </translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="936"/>
        <source>Show Log</source>
        <translation>Zeige Meldungen</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="936"/>
        <source>Show JSON</source>
        <translation>Zeige JSON</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="1136"/>
        <source>Load failed</source>
        <translation>Laden daneben</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="1188"/>
        <source>granted</source>
        <translation>gewährt</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="1188"/>
        <source>denied</source>
        <translation>abgelehnt</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="1190"/>
        <source>Never ask again</source>
        <translation>Nicht mehr fragen</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="1298"/>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="1299"/>
        <source>Select or Edit Options</source>
        <translation>Wähle oder Ändere Optionen</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="1300"/>
        <source>The Syntax is a JSON array with each element representing one command line argument.</source>
        <translation>Die Syntax ist ein JSON Feld mit jedem Element ein Kommandozeilenargument representierend.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="1306"/>
        <source>History</source>
        <translation>Geschichte</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="1306"/>
        <source>Select here previous commands for modification or repetition.</source>
        <translation>Wähle hier vorherige Kommandos zum Ändern oder Wiederholen.</translation>
    </message>
    <message>
        <location filename="../main2.cpp" line="100"/>
        <source>specify input JSON file; &apos;-&apos; stands for stdin stream</source>
        <translation>gebe eine JSON Eingangsdatei an; &apos;-&apos; steht für stdin Datenstrom</translation>
    </message>
    <message>
        <location filename="../main2.cpp" line="96"/>
        <source>QML Options Renderer</source>
        <translation>QML Optionen Betrachter</translation>
    </message>
    <message>
        <location filename="../main2.cpp" line="101"/>
        <location filename="../main2.cpp" line="105"/>
        <source>path/to/options.json</source>
        <translation>pfad/nach/optionen.json</translation>
    </message>
    <message>
        <location filename="../main2.cpp" line="104"/>
        <source>specify output JSON file; &apos;-&apos; stands for stdout stream</source>
        <translation>gebe eine JSON Ausgangsdatei an; &apos;-&apos; steht für stdout Datenstrom</translation>
    </message>
    <message>
        <location filename="../main2.cpp" line="108"/>
        <source>specify commands JSON file; &apos;-&apos; stands for stdin stream; &apos;+&apos; stands for read from -i file</source>
        <translation>gebe eine JSON Kommandodatei an; &apos;-&apos; steht für stdin Datenstrom; &apos;+&apos; steht für -i Datei</translation>
    </message>
    <message>
        <location filename="../main2.cpp" line="109"/>
        <source>path/to/commands.json</source>
        <translation>pfad/zu/kommando.json</translation>
    </message>
    <message>
        <location filename="../main2.cpp" line="112"/>
        <location filename="../main2.cpp" line="115"/>
        <source>Verbose mode. Prints out more information.</source>
        <translation>Plaudernd. Zeigt mehr Informationen.</translation>
    </message>
    <message>
        <location filename="../main2.cpp" line="133"/>
        <source>Example</source>
        <translation>Beispiel</translation>
    </message>
    <message>
        <location filename="../main2.cpp" line="134"/>
        <source>Open JSON from stdin and give result JSON to stdout:
		cat options.json | %1 -i - -o - | xargs echo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main2.cpp" line="135"/>
        <source>Open JSON from stdin and specify commands:
		cat options.json | %1 -i - -c commands.json</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main2.cpp" line="143"/>
        <source>is a QML renderer for Oyjl JSON options.</source>
        <translation>ist ein Betrachter für Oyjl JSON Optionen.</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <source>Set an interval; optional</source>
        <translation>Setze einen Zeitabstand; freiwillig</translation>
    </message>
    <message>
        <source>Set repeat interval in seconds.</source>
        <translation>Setze das Wiederholungsinterval in Sekunden.</translation>
    </message>
</context>
</TS>
