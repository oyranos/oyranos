_oyranos_policy()
{
    local cur prev words cword
    _init_completion -s || return

    #set -x -v

    local SEARCH=${COMP_WORDS[COMP_CWORD]}
    if [[ "$SEARCH" == "=" ]]; then
      SEARCH=""
    fi

    : "autocomplete options with choices for long options "$prev""
    case "$prev" in
        --render) # long option with static args
            local IFS=$'\n'
            local WORD_LIST=('gui' 'cli' 'web' '-')
            COMPREPLY=($(compgen -W '"${WORD_LIST[@]}"' -- "$cur"))
            set +x +v
            return
            ;;
        --help) # long option with static args
            local IFS=$'\n'
            local WORD_LIST=('-' 'synopsis')
            COMPREPLY=($(compgen -W '"${WORD_LIST[@]}"' -- "$cur"))
            set +x +v
            return
            ;;
        --export) # long option with static args
            local IFS=$'\n'
            local WORD_LIST=('man' 'markdown' 'json' 'json+command' 'export')
            COMPREPLY=($(compgen -W '"${WORD_LIST[@]}"' -- "$cur"))
            set +x +v
            return
            ;;
    esac
    : "autocomplete options with choices for single letter options "$cur""
    case "$cur" in
        -R=*) # single letter option with static args
            local IFS=$'\n'
            local WORD_LIST=('gui' 'cli' 'web' '-')
            COMPREPLY=($(compgen -W '"${WORD_LIST[@]}"' -- "$SEARCH"))
            set +x +v
            return
            ;;
        -h=*) # single letter option with static args
            local IFS=$'\n'
            local WORD_LIST=('-' 'synopsis')
            COMPREPLY=($(compgen -W '"${WORD_LIST[@]}"' -- "$SEARCH"))
            set +x +v
            return
            ;;
        -X=*) # single letter option with static args
            local IFS=$'\n'
            local WORD_LIST=('man' 'markdown' 'json' 'json+command' 'export')
            COMPREPLY=($(compgen -W '"${WORD_LIST[@]}"' -- "$SEARCH"))
            set +x +v
            return
            ;;
    esac


    : "autocomplete options "$cur""
    case "$cur" in
        -i|-s|-R|-h|-X)
            : "finish short options with choices"
            COMPREPLY=("$cur=\"")
            set +x +v
            return
            ;;
        --import-policy|--save-policy|--doc-title|--doc-version|--render|--help|--export)
            : "finish long options with choices"
            COMPREPLY=("$cur=\"")
            set +x +v
            return
            ;;
        -c|-f|-e|-l|-d|-p|-v|-V)
            : "finish short options without choices"
            COMPREPLY=("$cur ")
            set +x +v
            return
            ;;
        --current-policy|--file-name|--internal-name|--list-policies|--dump|--list-paths|--system-wide|--long-help|--docbook|--synopsis|--verbose|--version)
            : "finish long options without choices"
            COMPREPLY=("$cur ")
            set +x +v
            return
            ;;
    esac


    : "show help for none '@' UIs"
    if [[ "$cur" == "" ]]; then
      if [[ ${COMP_WORDS[1]} == "" ]]; then
        $1 help synopsis 1>&2
      else
        $1 help ${COMP_WORDS[1]} 1>&2
      fi
    fi


    : "suggest group options for subcommands"
    if [[ "$cur" == "" ]] || [[ "$cur" == "-" ]] || [[ "$cur" == -- ]] || [[ "$cur" == -* ]]; then
      case "${COMP_WORDS[1]}" in
        -c|--current-policy)
          COMPREPLY=($(compgen -W '-f --file-name -e --internal-name -v --verbose' -- "$cur"))
            set +x +v
            return
            ;;
        -l|--list-policies)
          COMPREPLY=($(compgen -W '-f --file-name -e --internal-name -v --verbose' -- "$cur"))
            set +x +v
            return
            ;;
        -d|--dump)
          COMPREPLY=($(compgen -W '-v --verbose' -- "$cur"))
            set +x +v
            return
            ;;
        -i|--import-policy)
          COMPREPLY=($(compgen -W '-v --verbose' -- "$cur"))
            set +x +v
            return
            ;;
        -p|--list-paths)
          COMPREPLY=($(compgen -W '-v --verbose' -- "$cur"))
            set +x +v
            return
            ;;
        -s|--save-policy)
          COMPREPLY=($(compgen -W '--system-wide -v --verbose' -- "$cur"))
            set +x +v
            return
            ;;
        --long-help|--docbook)
          COMPREPLY=($(compgen -W '--doc-title= --doc-version=' -- "$cur"))
            set +x +v
            return
            ;;
        -X|--export|-h|--help|-V|--version|-R|--render)
          COMPREPLY=($(compgen -W '-v --verbose' -- "$cur"))
            set +x +v
            return
            ;;
      esac
    fi

    : "suggest mandatory options on first args only"
    if [[ "${COMP_WORDS[2]}" == "" ]]; then
      local WORD_LIST=()
      WORD_LIST=("${WORD_LIST[@]}" -c --current-policy -l --list-policies -d --dump -i= --import-policy= -p --list-paths -s= --save-policy= --long-help --docbook -X= --export= -h= --help= -V --version -R= --render=)
      COMPREPLY=($(compgen -W '"${WORD_LIST[@]}"' -- "$cur"))
      set +x +v
      return
    fi

    set +x +v
} &&
complete -o nosort -F _oyranos_policy -o nospace oyranos-policy


