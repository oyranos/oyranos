/** @file  oyranos_git_version.h
 *  @brief automatic generated variables
 */
#ifndef OYRANOS_GIT_VERSION_H
#define OYRANOS_GIT_VERSION_H

#define OY_GIT_VERSION                 "0.9.6-4299-g2916472d6-2024-11-19"  /**< git describe --always HEAD  + date */ 


#endif /*OYRANOS_GIT_VERSION_H*/


