#define oyranos_json "{\
  \"org\": {\
    \"freedesktop\": {\
      \"oyjl\": {\
        \"translations\": {\
          \"cs_CZ\": {\
            \"Black Preservation\": \"Zachov?n? ?ern?\",\
            \"CMM\": \"CMM\",\
            \"Cmyk to Cmyk transforms can provide various strategies to preserve the black only channel. None means, black might change to Cmy and thus text prints not very well. LittleCMS has added two different modes to deal with that: Black-ink-only preservation and black-plane preservation. The first is simple and effective: do all the colorimetric transforms but keep only K (preserving L*) where the source image is only black. The second mode is fair more complex and tries to preserve the WHOLE K plane.\": \"P?i transformaci CMYK -> CMYK lze pou??t r?zn? strategie pro zachov?n? ?ern?ho kan?lu. '??dn?' znamen?, ?e ?ern? se m??e zm?nit na CMY a text se tud?? nevytiskne nejl?pe. Little CMS to ?e?? dv?ma zp?soby: zachov?n? pouze ?ern?ho inkoustu nebo zachov?n? roviny ?ern?. Prvn? zp?sob je jednoduch? a ??inn?: provedou se v?echny kolorimetrick? transformace, ale zachov? se K (sv?tlost L*) jen v p??padech, kdy vstupn? obr?zek je pouze ?ern?. Druh? zp?sob je zna?n? komplexn? a sna?? se zachovat celou rovinu ?ern? (K).\",\
            \"Color Transforms can be differently stored internally\": \"Transformace barev mohou b?t intern? ulo?eny r?zn?mi zp?soby\",\
            \"Copyright\": \"Copyright\",\
            \"Decide how to preserve the black channel for Cmyk to Cmyk transforms\": \"Rozhodnout, jak?m zp?sobem zachovat ?ern? kan?l p?i transformaci CMYK -> CMYK.\",\
            \"Documentation\": \"Dokumentace\",\
            \"Extended Options\": \"Roz???en? volby:\",\
            \"Kai-Uwe Behrmann\": \"Kai-Uwe Behrmann\",\
            \"License\": \"Licence\",\
            \"Little CMS\": \"Little CMS\",\
            \"Little CMS tries to optimize profile chains whatever possible. There are some built-in optimization schemes, and you can add new schemas by using a plug-in. This generally improves the performance of the transform, but may introduce a small delay of 1-2 seconds when creating the transform. If you are going to transform just few colors, you don't need this precalculations. Then, the flag cmsFLAGS_NOOPTIMIZE in cmsCreateTransform() can be used to inhibit the optimization process. See the API reference for a more detailed discussion of the flags.\": \"Little CMS se sna?? optimalizovat ?et?zen? profil? kdykoliv je to mo?n?. N?kter? optimaliza?n? sch?mata jsou ji? p?ednastavena, nov? sch?mata m??ete p?idat skrze plug-in. Obvykle dojde ke zlep?en? v?konu transformace, ale m??e doj?t ke zpo?d?n? p?i vytv??en? metody transformace. Pokud budete transformovat pouze n?kolik barev, pak nepot?ebujete tyto p?edb??n? v?po?ty. V takov?m p??pad? lze pou??t p??znak cmsFLAGS_NOOPTIMIZE v cmsCreateTransform() k zabr?n?n? procesu optimalizace. Viz reference API pro detailn?j?? diskuzi ohledn? p??znak?.\",\
            \"MIT\": \"MIT\",\
            \"Manufacturer\": \"V?robce\",\
            \"Marti Maria\": \"Marti Maria\",\
            \"Optimization\": \"Optimalizace\",\
            \"[none]\": \"[??dn?]\"\
          },\
          \"de_DE\": {\
            \"Additional options.\": \"Zus?tzliche Optionen.\",\
            \"Black Preservation\": \"Schwarzerhalt\",\
            \"CMM\": \"CMM\",\
            \"CMM behaviour options for color rendering and precission.\": \"CMM Verhalten f?r Farbumwandlung und Pr?zission.\",\
            \"Color Transforms can be differently stored internally\": \"Farbumwandlung k?nnen intern unterschiedlich gespeichert werden\",\
            \"Copyright\": \"Kopierrecht\",\
            \"Copyright (c) 1998-2008 Marti Maria Saguer\": \"Kopierrecht (c) 1998-2008 Marti Maria Saguer\",\
            \"Decide how to preserve the black channel for Cmyk to Cmyk transforms\": \"Entscheide wie der Schwarzkanal erhalten wird bei Umwandlungen von einem Druckfarbraum in einen Anderen\",\
            \"Development\": \"Entwicklung\",\
            \"Documentation\": \"Dokumente\",\
            \"Download\": \"Bezugsquelle\",\
            \"Extended Options\": \"Erweiterte Optionen\",\
            \"Kai-Uwe Behrmann\": \"Kai-Uwe Behrmann\",\
            \"LCMS_HIGHRESPRECALC\": \"LCMS_HIGHRESPRECALC\",\
            \"LCMS_LOWRESPRECALC\": \"LCMS_LOWRESPRECALC\",\
            \"LCMS_NOOPTIMIZE\": \"LCMS_NOOPTIMIZE\",\
            \"LCMS_PRESERVE_K_PLANE\": \"LCMS_PRESERVE_K_PLANE\",\
            \"LCMS_PRESERVE_PURE_K\": \"LCMS_PRESERVE_PURE_K\",\
            \"License\": \"Lizenz\",\
            \"Little CMS\": \"Little CMS\",\
            \"Little Color Management System\": \"Little Color Management System\",\
            \"MIT\": \"MIT\",\
            \"Manufacturer\": \"Hersteller\",\
            \"Marti Maria\": \"Marti Maria\",\
            \"Optimization\": \"Optimierung\",\
            \"Options\": \"Optionen\",\
            \"Oyjl Module Author\": \"Oyjl Modul Autor\",\
            \"Sources\": \"Quellen\",\
            \"Support\": \"Unterst?tzung\",\
            \"The lcms filter is a one by one color conversion filter. It can both create a color conversion context, some precalculated for processing speed up, and the color conversion with the help of that context. The adaption part of this filter transforms the Oyranos color context, which is ICC device link based, to the internal lcms format.\": \"Das lcms Modul kann eindimensional Farben ?bertragen. Die CMM, Farb?bertragungsmodul, berechnet die Farb?bertragungsdaten vor und kann Farben mit deren Hilfe auch ?bertragen. Es gibt einen Anpassungsteil im Modul, welcher die interne lcms Farb?bertragungsdaten an das Oyranos' Format anpasst, welches auf ICC Ger?teverkn?pfungen, engl. device links, beruht.\",\
            \"URL\": \"URL\",\
            \"[none]\": \"[kein]\",\
            \"http://www.behrmann.name\": \"http://www.behrmann.name\",\
            \"http://www.littlecms.com\": \"http://www.littlecms.com\",\
            \"http://www.littlecms.com/\": \"http://www.littlecms.com\",\
            \"http://www.littlecms.com/downloads.htm\": \"http://www.littlecms.com/downloads.htm\",\
            \"http://www.openicc.info\": \"http://www.openicc.info\",\
            \"http://www.opensource.org/licenses/mit-license.php\": \"http://www.opensource.org/licenses/mit-license.php\",\
            \"normal\": \"normal\",\
            \"stalled\": \"nicht aktiv\"\
          },\
          \"fr_FR\": {\
            \"Copyright\": \"Copyright\"\
          }\
        },\
        \"comment\": \"This is the Oyjl namespace\",\
        \"modules\": [{\
            \"comment\": \"This is the Oyjl-Module-API namespace\",\
            \"description\": \"Little Color Management System\",\
            \"groups\": [{\
                \"comment\": \"Logical group\",\
                \"description\": \"Extended Options\",\
                \"groups\": [{\
                    \"comment\": \"Logical sub group for presentation. *name* might be shown as label alone or together with *description*.\",\
                    \"description\": \"Options\",\
                    \"help\": \"CMM behaviour options for color rendering and precission.\",\
                    \"name\": \"CMM\",\
                    \"options\": [{\
                        \"choices\": [{\
                            \"name\": \"[none]\",\
                            \"nick\": \"0\"\
                          },{\
                            \"name\": \"LCMS_PRESERVE_PURE_K\",\
                            \"nick\": \"1\"\
                          },{\
                            \"name\": \"LCMS_PRESERVE_K_PLANE\",\
                            \"nick\": \"2\"\
                          }],\
                        \"default\": \"0\",\
                        \"description\": \"Decide how to preserve the black channel for Cmyk to Cmyk transforms\",\
                        \"help\": \"Cmyk to Cmyk transforms can provide various strategies to preserve the black only channel. None means, black might change to Cmy and thus text prints not very well. LittleCMS has added two different modes to deal with that: Black-ink-only preservation and black-plane preservation. The first is simple and effective: do all the colorimetric transforms but keep only K (preserving L*) where the source image is only black. The second mode is fair more complex and tries to preserve the WHOLE K plane.\",\
                        \"key\": \"org/freedesktop/openicc/icc_color/cmyk_cmyk_black_preservation\",\
                        \"name\": \"Black Preservation\",\
                        \"properties\": \"rendering\",\
                        \"type\": \"choice\"\
                      },{\
                        \"choices\": [{\
                            \"name\": \"normal\",\
                            \"nick\": \"0\"\
                          },{\
                            \"name\": \"LCMS_NOOPTIMIZE\",\
                            \"nick\": \"1\"\
                          },{\
                            \"name\": \"LCMS_HIGHRESPRECALC\",\
                            \"nick\": \"2\"\
                          },{\
                            \"name\": \"LCMS_LOWRESPRECALC\",\
                            \"nick\": \"3\"\
                          }],\
                        \"default\": \"0\",\
                        \"description\": \"Color Transforms can be differently stored internally\",\
                        \"help\": \"Little CMS tries to optimize profile chains whatever possible. There are some built-in optimization schemes, and you can add new schemas by using a plug-in. This generally improves the performance of the transform, but may introduce a small delay of 1-2 seconds when creating the transform. If you are going to transform just few colors, you don't need this precalculations. Then, the flag cmsFLAGS_NOOPTIMIZE in cmsCreateTransform() can be used to inhibit the optimization process. See the API reference for a more detailed discussion of the flags.\",\
                        \"key\": \"org/freedesktop/openicc/icc_color/precalculation\",\
                        \"name\": \"Optimization\",\
                        \"properties\": \"rendering.advanced\",\
                        \"type\": \"choice\"\
                      }],\
                    \"properties\": \"h4\"\
                  }],\
                \"help\": \"Additional options.\",\
                \"name\": \"Little CMS\",\
                \"properties\": \"h3.frame\"\
              }],\
            \"information\": [{\
                \"description\": \"http://www.littlecms.com\",\
                \"label\": \"Manufacturer\",\
                \"name\": \"Marti Maria\",\
                \"nick\": \"mm2\",\
                \"type\": \"manufacturer\"\
              },{\
                \"label\": \"Copyright\",\
                \"name\": \"Copyright (c) 1998-2008 Marti Maria Saguer\",\
                \"type\": \"copyright\"\
              },{\
                \"description\": \"http://www.opensource.org/licenses/mit-license.php\",\
                \"label\": \"License\",\
                \"name\": \"MIT\",\
                \"type\": \"license\"\
              },{\
                \"label\": \"URL\",\
                \"name\": \"http://www.littlecms.com\",\
                \"type\": \"url\"\
              },{\
                \"label\": \"Support\",\
                \"name\": \"http://www.littlecms.com/\",\
                \"type\": \"support\"\
              },{\
                \"label\": \"Download\",\
                \"name\": \"http://www.littlecms.com/\",\
                \"type\": \"download\"\
              },{\
                \"label\": \"Sources\",\
                \"name\": \"http://www.littlecms.com/downloads.htm\",\
                \"type\": \"sources\"\
              },{\
                \"label\": \"Development\",\
                \"name\": \"stalled\",\
                \"type\": \"development\"\
              },{\
                \"description\": \"http://www.behrmann.name\",\
                \"label\": \"Oyjl Module Author\",\
                \"name\": \"Kai-Uwe Behrmann\",\
                \"type\": \"oyjl_module_author\"\
              },{\
                \"description\": \"The lcms filter is a one by one color conversion filter. It can both create a color conversion context, some precalculated for processing speed up, and the color conversion with the help of that context. The adaption part of this filter transforms the Oyranos color context, which is ICC device link based, to the internal lcms format.\",\
                \"label\": \"Documentation\",\
                \"name\": \"http://www.openicc.info\",\
                \"type\": \"documentation\"\
              }],\
            \"label\": \"CMM\",\
            \"logo\": \"lcms_logo2\",\
            \"name\": \"Little CMS\",\
            \"nick\": \"lcms\",\
            \"oyjl_module_api_version\": \"1\",\
            \"type\": \"CMM\"\
          }]\
      }\
    }\
  }\
}"
