#define oyranos_json "{\
  \"org\": {\
    \"freedesktop\": {\
      \"oyjl\": {\
        \"translations\": {\
          \"cs_CZ\": {\
            \"Adaptation State\": \"Stav adaptace\",\
            \"Adaptation state for absolute colorimetric intent\": \"Adapta?n? stav pro absolutn? kolorimetrick? z?m?r\",\
            \"Black Preservation\": \"Zachov?n? ?ern?\",\
            \"CMM\": \"CMM\",\
            \"Cmyk to Cmyk transforms can provide various strategies to preserve the black only channel. None means, black might change to Cmy and thus text prints not very well. LittleCMS 2 has added two different modes to deal with that: Black-ink-only preservation and black-plane preservation. The first is simple and effective: do all the colorimetric transforms but keep only K (preserving L*) where the source image is only black. The second mode is fair more complex and tries to preserve the WHOLE K plane.\": \"P?i transformaci CMYK -> CMYK lze pou??t r?zn? strategie pro zachov?n? ?ern?ho kan?lu. '??dn?' znamen?, ?e ?ern? se m??e zm?nit na CMY a text se tedy nevytiskne nejl?pe. LittleCMS 2 to ?e?? dv?ma zp?soby: zachov?n? pouze ?ern?ho inkoustu nebo zachov?n? roviny ?ern?. Prvn? zp?sob je jednoduch? a ??inn?: provedou se v?echny kolorimetrick? transformace, ale zachov? se jen K (sv?tlost L*) v p??padech, kdy vstupn? obr?zek je pouze ?ern?. Druh? zp?sob je zna?n? komplexn? a sna?? se zachovat celou rovinu ?ern? (K).\",\
            \"Color Transform CLUT's can additionally use curves for special cases\": \"Transformace barev typu CLUT m??e ve zvl??tn?ch p??padech vyu??t i k?ivky.\",\
            \"Color Transforms can be differently stored internally\": \"Transformace barev mohou b?t intern? ulo?eny r?zn?mi zp?soby\",\
            \"Copyright\": \"Copyright\",\
            \"Curves for Optimization\": \"K?ivky pro optimalizaci\",\
            \"Decide how to preserve the black channel for Cmyk to Cmyk transforms\": \"Rozhodnout, jak?m zp?sobem zachovat ?ern? kan?l p?i transformaci CMYK -> CMYK.\",\
            \"Documentation\": \"Dokumentace\",\
            \"Kai-Uwe Behrmann\": \"Kai-Uwe Behrmann\",\
            \"License\": \"Licence\",\
            \"Little CMS 2\": \"Little CMS 2\",\
            \"Little CMS can use curves before and after CLUT's for special cases like gamma encoded values to and from linear gamma values. Performance will suffer.\": \"Ve zvl??tn?ch p??padech jako hodnoty k?dovan? v gama do a z line?rn?ch hodnot m??e Little CMS vyu??t k?ivky p?ed a po CLUT. V?kon v?ak poklesne.\",\
            \"Little CMS tries to optimize profile chains whatever possible. There are some built-in optimization schemes, and you can add new schemas by using a plug-in. This generally improves the performance of the transform, but may introduce a small delay of 1-2 seconds when creating the transform. If you are going to transform just few colors, you don't need this precalculations. Then, the flag cmsFLAGS_NOOPTIMIZE in cmsCreateTransform() can be used to inhibit the optimization process. See the API reference for a more detailed discussion of the flags.\": \"Little CMS se sna?? optimalizovat ?et?zen? profil? kdykoliv je to mo?n?. N?kter? optimaliza?n? sch?mata jsou ji? p?ednastavena, nov? sch?mata m??ete p?idat skrze plug-in. Obvykle dojde ke zlep?en? v?konu transformace, ale m??e doj?t ke zpo?d?n? p?i vytv??en? metody transformace. Pokud budete transformovat pouze n?kolik barev, pak nepot?ebujete tyto p?edb??n? v?po?ty. V takov?m p??pad? lze pou??t p??znak cmsFLAGS_NOOPTIMIZE v cmsCreateTransform() k zabr?n?n? procesu optimalizace. Viz reference API pro detailn?j?? diskuzi ohledn? p??znak?.\",\
            \"MIT\": \"MIT\",\
            \"Manufacturer\": \"V?robce\",\
            \"Marti Maria\": \"Marti Maria\",\
            \"No\": \"Ne\",\
            \"Optimization\": \"Optimalizace\",\
            \"The adaption state should be between 0 and 1.0 and will apply to the absolute colorimetric intent.\": \"Adapta?n? stav by m?l b?t mezi 0 a 1.0 a t?k? se absolutn?ho kolorimetrick?ho z?m?ru.\",\
            \"Yes\": \"Ano\",\
            \"[none]\": \"[??dn?]\"\
          },\
          \"de_DE\": {\
            \"0.0\": \"0,0\",\
            \"1.0\": \"1,0\",\
            \"Adaptation State\": \"Farbanpassungsgrad\",\
            \"Adaptation state for absolute colorimetric intent\": \"Farbanpassungsgrad f?r die absolut farbmetrische ?bertragung\",\
            \"Avoid force of White on White mapping. Default for absolute rendering intent.\": \"Lasse das Erzwingen einer Weiss zu Weiss ?bertragung fort. Standard bei Absolut Farbmetrischer ?bertragung.\",\
            \"Black Preservation\": \"Schwarzerhalt\",\
            \"CMM\": \"CMM\",\
            \"CMM options for color rendering, precission and fixes.\": \"CMM Optionen f?r Farbumwandlung, Pr?zission und Verbesserungen.\",\
            \"Color Transform CLUT's can additionally use curves for special cases\": \"Farbtransformationstabellen k?nnen f?r spezielle F?lle zus?tzlich mit Kurven kombiniert werden\",\
            \"Color Transforms can be differently stored internally\": \"Farbumwandlung k?nnen intern unterschiedlich gespeichert werden\",\
            \"Copyright\": \"Kopierrecht\",\
            \"Copyright 2018 Marti Maria\": \"Kopierrecht 2018 Marti Maria\",\
            \"Curves for Optimization\": \"Kurven f?r Optimierungen\",\
            \"Decide how to preserve the black channel for Cmyk to Cmyk transforms\": \"Entscheide wie der Schwarzkanal erhalten wird bei Umwandlungen von einem Druckfarbraum in einen Anderen\",\
            \"Development\": \"Entwicklung\",\
            \"Documentation\": \"Dokumente\",\
            \"Download\": \"Bezugsquelle\",\
            \"Kai-Uwe Behrmann\": \"Kai-Uwe Behrmann\",\
            \"LCMS2_HIGHRESPRECALC\": \"LCMS2_HIGHRESPRECALC\",\
            \"LCMS2_LOWRESPRECALC\": \"LCMS2_LOWRESPRECALC\",\
            \"LCMS2_NOOPTIMIZE\": \"LCMS2_NOOPTIMIZE\",\
            \"LCMS2_NOTRANSFORM\": \"LCMS2_NOTRANSFORM\",\
            \"LCMS2_POST+PRE_CURVES\": \"LCMS2_POST+PRE_CURVES\",\
            \"LCMS_PRESERVE_K_PLANE\": \"LCMS_PRESERVE_K_PLANE\",\
            \"LCMS_PRESERVE_PURE_K\": \"LCMS_PRESERVE_PURE_K\",\
            \"License\": \"Lizenz\",\
            \"Little CMS 2\": \"Little CMS 2\",\
            \"Little CMS can use curves before and after CLUT's for special cases like gamma encoded values to and from linear gamma values. Performance will suffer.\": \"Little CMS kann Kurven vor und nach 3D Interpolationstabellen benutzen. Dadurch zeigen sich weniger Fehler bei der ?bertragung zwischen linearen und gammabehafteten Werten auf Kosten der Geschwindigkeit.\",\
            \"Little Color Management System\": \"Little Color Management System\",\
            \"MIT\": \"MIT\",\
            \"Manufacturer\": \"Hersteller\",\
            \"Marti Maria\": \"Marti Maria\",\
            \"No\": \"Nein\",\
            \"No White on White Fix\": \"Keine Weiss zu Weiss Reparatur\",\
            \"Optimization\": \"Optimierung\",\
            \"Oyjl Module Author\": \"Oyjl Modul Autor\",\
            \"Set lcm2 Options\": \"Editiere lcm2 Optionen\",\
            \"Skip White Point on White point alignment\": \"Lasse Weiss auf Weiss Einpassung weg\",\
            \"Sources\": \"Quellen\",\
            \"Support\": \"Unterst?tzung\",\
            \"The adaption state should be between 0 and 1.0 and will apply to the absolute colorimetric intent.\": \"Der Farbanpassungsgrad sollte zwischen 0 und 1.0 liegen und wird auf die absolut farbmetrische ?bertragung angewendet.\",\
            \"The module expects options for profiles and rendering_intent. Optional are rendering_bpc, cmyk_cmyk_black_preservation, precalculation_curves, adaption_state, no_white_on_white_fixup.\": \"Das Modul erwartet Optionen f?r Profile und ?bertragungsfunktion. Optional sind rendering_bpc, cmyk_cmyk_black_preservation, precalculation_curves, adaption_state, no_white_on_white_fixup.\",\
            \"URL\": \"URL\",\
            \"Yes\": \"Ja\",\
            \"[none]\": \"[kein]\",\
            \"http://www.behrmann.name\": \"http://www.behrmann.name\",\
            \"http://www.littlecms.com\": \"http://www.littlecms.com\",\
            \"http://www.littlecms.com/\": \"http://www.littlecms.com\",\
            \"http://www.openicc.info\": \"http://www.openicc.info\",\
            \"https://github.com/mm2/little-cms\": \"https://github.com/mm2/little-cms\",\
            \"lcm2 Specific Options\": \"lcm2 Spezifische Optionen\",\
            \"normal\": \"normal\"\
          },\
          \"eo\": {\
            \"Yes\": \"Jes\"\
          },\
          \"fr_FR\": {\
            \"Copyright\": \"Copyright\"\
          }\
        },\
        \"comment\": \"This is the Oyjl namespace\",\
        \"modules\": [{\
            \"comment\": \"This is the Oyjl-Module-API namespace\",\
            \"description\": \"Little Color Management System\",\
            \"groups\": [{\
                \"comment\": \"Logical group\",\
                \"groups\": [{\
                    \"comment\": \"Logical sub group for presentation. *name* might be shown as label alone or together with *description*.\",\
                    \"description\": \"Set lcm2 Options\",\
                    \"help\": \"CMM options for color rendering, precission and fixes.\",\
                    \"name\": \"lcm2 Specific Options\",\
                    \"options\": [{\
                        \"choices\": [{\
                            \"name\": \"[none]\",\
                            \"nick\": \"0\"\
                          },{\
                            \"name\": \"LCMS_PRESERVE_PURE_K\",\
                            \"nick\": \"1\"\
                          },{\
                            \"name\": \"LCMS_PRESERVE_K_PLANE\",\
                            \"nick\": \"2\"\
                          }],\
                        \"default\": \"0\",\
                        \"description\": \"Decide how to preserve the black channel for Cmyk to Cmyk transforms\",\
                        \"help\": \"Cmyk to Cmyk transforms can provide various strategies to preserve the black only channel. None means, black might change to Cmy and thus text prints not very well. LittleCMS 2 has added two different modes to deal with that: Black-ink-only preservation and black-plane preservation. The first is simple and effective: do all the colorimetric transforms but keep only K (preserving L*) where the source image is only black. The second mode is fair more complex and tries to preserve the WHOLE K plane.\",\
                        \"key\": \"org/freedesktop/openicc/icc_color/cmyk_cmyk_black_preservation\",\
                        \"name\": \"Black Preservation\",\
                        \"properties\": \"rendering\",\
                        \"type\": \"choice\"\
                      },{\
                        \"choices\": [{\
                            \"name\": \"normal\",\
                            \"nick\": \"0\"\
                          },{\
                            \"name\": \"LCMS2_NOOPTIMIZE\",\
                            \"nick\": \"1\"\
                          },{\
                            \"name\": \"LCMS2_HIGHRESPRECALC\",\
                            \"nick\": \"2\"\
                          },{\
                            \"name\": \"LCMS2_LOWRESPRECALC\",\
                            \"nick\": \"3\"\
                          },{\
                            \"name\": \"LCMS2_NOTRANSFORM\",\
                            \"nick\": \"4\"\
                          }],\
                        \"default\": \"0\",\
                        \"description\": \"Color Transforms can be differently stored internally\",\
                        \"help\": \"Little CMS tries to optimize profile chains whatever possible. There are some built-in optimization schemes, and you can add new schemas by using a plug-in. This generally improves the performance of the transform, but may introduce a small delay of 1-2 seconds when creating the transform. If you are going to transform just few colors, you don't need this precalculations. Then, the flag cmsFLAGS_NOOPTIMIZE in cmsCreateTransform() can be used to inhibit the optimization process. See the API reference for a more detailed discussion of the flags.\",\
                        \"key\": \"org/freedesktop/openicc/icc_color/precalculation\",\
                        \"name\": \"Optimization\",\
                        \"properties\": \"rendering.advanced\",\
                        \"type\": \"choice\"\
                      },{\
                        \"choices\": [{\
                            \"name\": \"[none]\",\
                            \"nick\": \"0\"\
                          },{\
                            \"name\": \"LCMS2_POST+PRE_CURVES\",\
                            \"nick\": \"1\"\
                          }],\
                        \"default\": \"1\",\
                        \"description\": \"Color Transform CLUT's can additionally use curves for special cases\",\
                        \"help\": \"Little CMS can use curves before and after CLUT's for special cases like gamma encoded values to and from linear gamma values. Performance will suffer.\",\
                        \"key\": \"org/freedesktop/openicc/icc_color/precalculation_curves\",\
                        \"name\": \"Curves for Optimization\",\
                        \"properties\": \"rendering.advanced.boolean\",\
                        \"type\": \"choice\"\
                      },{\
                        \"choices\": [{\
                            \"name\": \"0.0\",\
                            \"nick\": \"0\"\
                          },{\
                            \"name\": \"1.0\",\
                            \"nick\": \"1\"\
                          }],\
                        \"default\": \"1\",\
                        \"description\": \"Adaptation state for absolute colorimetric intent\",\
                        \"help\": \"The adaption state should be between 0 and 1.0 and will apply to the absolute colorimetric intent.\",\
                        \"key\": \"org/freedesktop/openicc/icc_color/adaption_state\",\
                        \"name\": \"Adaptation State\",\
                        \"properties\": \"rendering.advanced\",\
                        \"type\": \"choice\"\
                      },{\
                        \"choices\": [{\
                            \"name\": \"No\",\
                            \"nick\": \"0\"\
                          },{\
                            \"name\": \"Yes\",\
                            \"nick\": \"1\"\
                          }],\
                        \"default\": \"1\",\
                        \"description\": \"Skip White Point on White point alignment\",\
                        \"help\": \"Avoid force of White on White mapping. Default for absolute rendering intent.\",\
                        \"key\": \"org/freedesktop/openicc/icc_color/no_white_on_white_fixup\",\
                        \"name\": \"No White on White Fix\",\
                        \"properties\": \"rendering.advanced\",\
                        \"type\": \"bool\"\
                      }],\
                    \"properties\": \"h4\"\
                  }],\
                \"name\": \"Little CMS 2\",\
                \"properties\": \"h3.frame\"\
              }],\
            \"information\": [{\
                \"description\": \"http://www.littlecms.com\",\
                \"label\": \"Manufacturer\",\
                \"name\": \"Marti Maria\",\
                \"nick\": \"mm2\",\
                \"type\": \"manufacturer\"\
              },{\
                \"label\": \"Copyright\",\
                \"name\": \"Copyright 2018 Marti Maria\",\
                \"type\": \"copyright\"\
              },{\
                \"description\": \"http://www.littlecms.com\",\
                \"label\": \"License\",\
                \"name\": \"MIT\",\
                \"type\": \"license\"\
              },{\
                \"label\": \"URL\",\
                \"name\": \"http://www.littlecms.com\",\
                \"type\": \"url\"\
              },{\
                \"label\": \"Support\",\
                \"name\": \"http://www.littlecms.com/\",\
                \"type\": \"support\"\
              },{\
                \"label\": \"Download\",\
                \"name\": \"http://www.littlecms.com/\",\
                \"type\": \"download\"\
              },{\
                \"label\": \"Sources\",\
                \"name\": \"http://www.littlecms.com/\",\
                \"type\": \"sources\"\
              },{\
                \"label\": \"Development\",\
                \"name\": \"https://github.com/mm2/little-cms\",\
                \"type\": \"development\"\
              },{\
                \"description\": \"http://www.behrmann.name\",\
                \"label\": \"Oyjl Module Author\",\
                \"name\": \"Kai-Uwe Behrmann\",\
                \"type\": \"oyjl_module_author\"\
              },{\
                \"description\": \"The module expects options for profiles and rendering_intent. Optional are rendering_bpc, cmyk_cmyk_black_preservation, precalculation_curves, adaption_state, no_white_on_white_fixup.\",\
                \"label\": \"Documentation\",\
                \"name\": \"http://www.openicc.info\",\
                \"type\": \"documentation\"\
              }],\
            \"label\": \"CMM\",\
            \"logo\": \"lcms_logo2\",\
            \"name\": \"Little CMS 2\",\
            \"nick\": \"lcm2\",\
            \"oyjl_module_api_version\": \"1\",\
            \"type\": \"CMM\"\
          }]\
      }\
    }\
  }\
}"
