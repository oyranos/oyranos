# oyranos\-config\-synnefo  {#oyranosconfigsynnefo}
[NAME](#name) [SYNOPSIS](#synopsis) [DESCRIPTION](#description) [OPTIONS](#options) [ENVIRONMENT](#environment) [SEE ALSO](#seealso) [AUTHOR](#author) [BUGS](#bugs) 

*"oyranos\-config\-synnefo"* *1* *""* "User Commands"
## NAME <a name="name"></a>
oyranos\-config\-synnefo  \- Color Management Configuration.
## SYNOPSIS <a name="synopsis"></a>
**oyranos\-config\-synnefo**
## DESCRIPTION <a name="description"></a>
oyranos\-config\-synnefo uses the Oyranos CMS to provide the ability to set and examine device profiles, as well as to change system\-wide color settings.
## OPTIONS <a name="options"></a>
&nbsp;&nbsp;**oyranos\-config\-synnefo**

<table style='width:100%'>
 <tr><td style='padding\-left:1em;padding-right:1em;vertical-align:top;width:25%'>|<strong>--</strong></td> <td>No args<br />Run command without arguments</td> </tr>
</table>

## ENVIRONMENT <a name="environment"></a>
### DISPLAY
&nbsp;&nbsp;On X11 systems the display is selected by this variable.
## SEE ALSO <a name="seealso"></a>
###  [oyranos-config\-fltk](oyranosconfigfltk.html)<a href="oyranosconfigfltk.md">(1)</a>  [oyranos](oyranos.html)<a href="oyranos.md">(3)</a>
## AUTHOR <a name="author"></a>
Joseph Simon \- main author Kai\-Uwe Behrmann (ku.b (at) gmx.de) \- maintainer 
## BUGS <a name="bugs"></a>
oyranos\-config\-synnefo                                oyranos\-config\-synnefo(1)(April 05, 2017) [http://sourceforge.net/tracker/?group\_id=177017&atid=879553](http://sourceforge.net/tracker/?group\_id=177017&atid=879553)

