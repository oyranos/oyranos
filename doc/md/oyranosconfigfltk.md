# oyranos\-config\-fltk  {#oyranosconfigfltk}
[NAME](#name) [SYNOPSIS](#synopsis) [DESCRIPTION](#description) [OPTIONS](#options) [EXAMPLES](#examples) [SEE ALSO](#seealso) [AUTHOR](#author) [BUGS](#bugs) 

*"oyranos\-config\-fltk"* *1* *"April 05, 2017"* "User Commands"
## NAME <a name="name"></a>
oyranos\-config\-fltk  \- Oyranos CMS configuration panel
## SYNOPSIS <a name="synopsis"></a>
**oyranos\-config\-fltk**
## DESCRIPTION <a name="description"></a>
The dialog lets you access most of the Oyranos color management system (CMS) dynamic informations.
## OPTIONS <a name="options"></a>
&nbsp;&nbsp;**oyranos\-config\-fltk**

<table style='width:100%'>
 <tr><td style='padding\-left:1em;padding-right:1em;vertical-align:top;width:25%'>|<strong>--</strong></td> <td>No args<br />Run command without arguments</td> </tr>
</table>

## EXAMPLES <a name="examples"></a>
### www.oyranos.org/wiki/index.php?title=Oyranos\_Configuration\_Dialog
## SEE ALSO <a name="seealso"></a>
###  [oyranos-config](oyranosconfig.html)<a href="oyranosconfig.md">(3)</a>  [oyranos\-config\-synnefo](oyranosconfigsynnefo.html)<a href="oyranosconfigsynnefo.md">(1)</a>  [oyranos\-policy](oyranospolicy.html)<a href="oyranospolicy.md">(1)</a>  [oyranos\-monitor](oyranosmonitor.html)<a href="oyranosmonitor.md">(1)</a>  [oyranos](oyranos.html)<a href="oyranos.md">(3)</a>
## AUTHOR <a name="author"></a>
Kai\-Uwe Behrmann (ku.b (at) gmx.de) 
## BUGS <a name="bugs"></a>
at: [https://github.com/oyranos\-cms/oyranos/issues](https://github.com/oyranos\-cms/oyranos/issues)

